component accessors=true {

	property name='credentials' type='com.amazonaws.auth.BasicAWSCredentials' getter='false' setter='false';
	property name='credentialsProvider' type='com.amazonaws.auth.AWSStaticCredentialsProvider' getter='false' setter='false';
	property name='regions' type='com.amazonaws.regions.Regions' getter='false' setter='false';
	property name='region' type='string' getter='false' setter='false';

	public aws function init(required string account, required string secret, string region='us-east-1')
	{
		variables.credentials = createAWSObject('auth.BasicAWSCredentials').init(arguments.account, arguments.secret);
		variables.credentialsProvider = createAWSObject('auth.AWSStaticCredentialsProvider').init(variables.credentials);
		variables.regions = createAWSObject('regions.Regions');
		variables.region = arguments.region;

		return this;
	}

	private function createAWSObject(required string name)
	{
		return createObject( 'java', 'com.amazonaws.' & arguments.name, [ getDirectoryFromPath( getCurrentTemplatePath() ) & "/lib"] );
	}

	private function getCredentials()
	{
		return variables.credentials;
	}

	private function getCredentialsProvider()
	{
		return variables.credentialsProvider;
	}

	private function getRegions()
	{
		return variables.regions;
	}

	public function getRegion()
	{
		return variables.region;
	}

	private any function getMyClient()
	{
		return variables.myClient;
	}

	public function setRegion(required string region)
	{
		getMyClient().configureRegion( getRegion(arguments.region) );

		return this;
	}

}