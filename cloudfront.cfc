component accessors=true extends='aws' {

	property name='myClient' type='com.amazonaws.services.s3.AmazonS3Client' getter=false setter=false;
	property name='bucketACL' type='com.amazonaws.services.s3.model.AccessControlList' getter=false setter=false;
	property name='bucket' type='string' getter=true setter=false;
	property name='basepath' type='string' getter=true setter=false;
	property name='keyPairID' type='string' getter=true setter=false;
	property name='privateKeyFile' type='string' getting=true setter=true;

	public cloudfront function init(required string account, required string secret, string region='us-east-1', required string bucket, string basepath = '') {

		super.init(argumentCollection = arguments);

		var builder = createAWSObject('services.cloudfront.AmazonCloudFrontClientBuilder').standard()
			.withCredentials( getCredentialsProvider() )
			.withRegion(arguments.region);

		variables.myClient = builder.build();

		variables.cloudFrontUrlSigner = createAWSObject( 'services.cloudfront.CloudFrontUrlSigner' );
		variables.privateKeyFile = createObject( 'java', 'java.io.File' ).init( application.environment.AWS_CLOUD_FRONT_KEY_PATH );
		variables.signerProtocol = createAWSObject( 'services.cloudfront.util.SignerUtils$Protocol' ).https;

		variables.bucket = arguments.bucket;
		variables.basepath = arguments.basepath;

		return this;
	}

	public string function getKeyFromPath(required string key) {
		return variables.basepath & arguments.key;
	}

	/**
	* @hint Returns a new URL signer. This is used for generating presigned URLs
	* @output false
	*/
	public any function getCloudFrontUrlSigner(){
		return variables.cloudFrontUrlSigner;
	}

	/**
	* @hint Provide a presigned expiring URL for a request.
	* @output false
	* @s3Key The S3 key for which you want to request a URL
	* @cloudFrontDomain The domain for which you want to request a URL
	* @ttl The number of minutes the Url will be alive. The default is 5 minutes.
	*/
	public string function generatePresignedUrl(
		required string s3Key,
		string cloudFrontDomain = application.environment.AWS_CLOUD_FRONT_FILES_DOMAIN,
		numeric ttl = 5
	) {

		return variables.cloudFrontUrlSigner.getSignedURLWithCannedPolicy(
			variables.signerProtocol,
			arguments.cloudFrontDomain,
			variables.privateKeyFile,
			this.formatS3Key( arguments.s3Key ),
			application.environment.AWS_CLOUD_FRONT_KEY_PAIR_ID,
			dateAdd( 'n', arguments.ttl, now() )
		).toString();

	}

	/**
	 * Url escapes the file name in the S3 object key stirng
	 *
	 * @s3Key The S3 key for which you want to request a URL
	 */
	private string function formatS3Key(required string s3Key){
		// Url escape the file name, excluding the extension with period at the end. This is the S3 object key format.
		var fileName = listLast( arguments.s3Key, '/' );
		var escapedFileName = urlEncodedFormat( fileName );
		
		// Convert safe characters back because S3 needs them
		// !, -, _, ., *, ', (, and )
		escapedFileName = replace( escapedFileName, '%20', '+', 'all' );
		escapedFileName = replace( escapedFileName, '%2D', '-', 'all' );
		escapedFileName = replace( escapedFileName, '%5F', '_', 'all' );			
		escapedFileName = replace( escapedFileName, '%2E', '.', 'all' );

		// escapedFileName = replace( escapedFileName, '%21', '!', 'all' );
		// escapedFileName = replace( escapedFileName, '%2A', '*', 'all' );
		// escapedFileName = replace( escapedFileName, '%27', "'", 'all' );
		// escapedFileName = replace( escapedFileName, '%28', '(', 'all' );
		// escapedFileName = replace( escapedFileName, '%29', ')', 'all' );

		return replace( arguments.s3Key, fileName, escapedFileName );
	}
}