# AWS SDK for Java

Package to provide simpler access to common AWS commands through the AWS SDK.

## Prerequisites

* Install Docker: https://docs.docker.com/docker-for-mac/install/
* Install CommandBox (With JRE included): https://www.ortussolutions.com/products/commandbox#download

## Environment Setup

### Docker
The `docker-compose.yml` file contains several variable references which configure environment variables for the various services. To set these variables, create a `.env` file in the root of the project with the following structure:
```
AWS_AKI=
AWS_SAK=
AWS_CLOUD_FRONT_KEY_PATH=
AWS_CLOUD_FRONT_KEY_PAIR_ID=
AWS_CLOUD_FRONT_KEY_FILE=
AWS_CLOUD_FRONT_FILES_DOMAIN=
AWS_S3_FILES_BUCKET=
```
Contact your team lead and request the settings/credentials necessary to complete the installation of the `.env` file.

### CommandBox Packages
From the root directory of the project, run `box install` to install all package dependencies.

## Docker Build

Build and start the docker services

```
docker-compose up -d --build
```

## Site Location

Once the container is up, you can use the following link to access the application:

* http://localhost:62383/tests