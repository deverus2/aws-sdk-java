component accessors=true extends='aws' {

	property name='myClient' type='com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient' getter='false' setter='false';	
	property name='dynamodb' type='com.amazonaws.services.dynamodbv2.document.DynamoDB' getter='false' setter='false';	

	property name='tables' type='struct' getter='false' setter='false';
	property name='reservedWords' type='array' setter='false';

	public dynamodb function init(required string account, required string secret, string region)
	{

		super.init(argumentCollection = arguments);

		variables.tables = {};

		var builder = createAWSObject('services.dynamodbv2.AmazonDynamoDBClientBuilder').standard()
			.withCredentials( getCredentialsProvider() )
			.withRegion(arguments.region);
			
		variables.myClient = builder.build();

		variables.dynamodb = CreateAWSObject( 'services.dynamodbv2.document.DynamoDB' ).init( getMyClient() );

		// NOTE: We're not currently using this list, but i went throught all the trouble of adding it, so I'm leaving it here
		variables.reservedWords = [
			'ABORT', 'ABSOLUTE', 'ACTION', 'ADD', 'AFTER', 'AGENT', 'AGGREGATE', 'ALL', 'ALLOCATE', 'ALTER', 'ANALYZE', 'AND', 'ANY', 'ARCHIVE', 'ARE', 'ARRAY', 'AS', 'ASC', 'ASCII', 'ASENSITIVE', 
			'ASSERTION', 'ASYMMETRIC', 'AT', 'ATOMIC', 'ATTACH', 'ATTRIBUTE', 'AUTH', 'AUTHORIZATION', 'AUTHORIZE', 'AUTO', 'AVG', 'BACK', 'BACKUP', 'BASE', 'BATCH', 'BEFORE', 'BEGIN', 'BETWEEN', 
			'BIGINT', 'BINARY', 'BIT', 'BLOB', 'BLOCK', 'BOOLEAN', 'BOTH', 'BREADTH', 'BUCKET', 'BULK', 'BY', 'BYTE', 'CALL', 'CALLED', 'CALLING', 'CAPACITY', 'CASCADE', 'CASCADED', 'CASE', 'CAST', 
			'CATALOG', 'CHAR', 'CHARACTER', 'CHECK', 'CLASS', 'CLOB', 'CLOSE', 'CLUSTER', 'CLUSTERED', 'CLUSTERING', 'CLUSTERS', 'COALESCE', 'COLLATE', 'COLLATION', 'COLLECTION', 'COLUMN', 'COLUMNS', 
			'COMBINE', 'COMMENT', 'COMMIT', 'COMPACT', 'COMPILE', 'COMPRESS', 'CONDITION', 'CONFLICT', 'CONNECT', 'CONNECTION', 'CONSISTENCY', 'CONSISTENT', 'CONSTRAINT', 'CONSTRAINTS', 'CONSTRUCTOR', 
			'CONSUMED', 'CONTINUE', 'CONVERT', 'COPY', 'CORRESPONDING', 'COUNT', 'COUNTER', 'CREATE', 'CROSS', 'CUBE', 'CURRENT', 'CURSOR', 'CYCLE', 'DATA', 'DATABASE', 'DATE', 'DATETIME', 'DAY', 
			'DEALLOCATE', 'DEC', 'DECIMAL', 'DECLARE', 'DEFAULT', 'DEFERRABLE', 'DEFERRED', 'DEFINE', 'DEFINED', 'DEFINITION', 'DELETE', 'DELIMITED', 'DEPTH', 'DEREF', 'DESC', 'DESCRIBE', 'DESCRIPTOR', 
			'DETACH', 'DETERMINISTIC', 'DIAGNOSTICS', 'DIRECTORIES', 'DISABLE', 'DISCONNECT', 'DISTINCT', 'DISTRIBUTE', 'DO', 'DOMAIN', 'DOUBLE', 'DROP', 'DUMP', 'DURATION', 'DYNAMIC', 'EACH', 'ELEMENT', 
			'ELSE', 'ELSEIF', 'EMPTY', 'ENABLE', 'END', 'EQUAL', 'EQUALS', 'ERROR', 'ESCAPE', 'ESCAPED', 'EVAL', 'EVALUATE', 'EXCEEDED', 'EXCEPT', 'EXCEPTION', 'EXCEPTIONS', 'EXCLUSIVE', 'EXEC', 'EXECUTE', 
			'EXISTS', 'EXIT', 'EXPLAIN', 'EXPLODE', 'EXPORT', 'EXPRESSION', 'EXTENDED', 'EXTERNAL', 'EXTRACT', 'FAIL', 'FALSE', 'FAMILY', 'FETCH', 'FIELDS', 'FILE', 'FILTER', 'FILTERING', 'FINAL', 
			'FINISH', 'FIRST', 'FIXED', 'FLATTERN', 'FLOAT', 'FOR', 'FORCE', 'FOREIGN', 'FORMAT', 'FORWARD', 'FOUND', 'FREE', 'FROM', 'FULL', 'FUNCTION', 'FUNCTIONS', 'GENERAL', 'GENERATE', 'GET', 'GLOB', 
			'GLOBAL', 'GO', 'GOTO', 'GRANT', 'GREATER', 'GROUP', 'GROUPING', 'HANDLER', 'HASH', 'HAVE', 'HAVING', 'HEAP', 'HIDDEN', 'HOLD', 'HOUR', 'IDENTIFIED', 'IDENTITY', 'IF', 'IGNORE', 'IMMEDIATE', 
			'IMPORT', 'IN', 'INCLUDING', 'INCLUSIVE', 'INCREMENT', 'INCREMENTAL', 'INDEX', 'INDEXED', 'INDEXES', 'INDICATOR', 'INFINITE', 'INITIALLY', 'INLINE', 'INNER', 'INNTER', 'INOUT', 'INPUT', 
			'INSENSITIVE', 'INSERT', 'INSTEAD', 'INT', 'INTEGER', 'INTERSECT', 'INTERVAL', 'INTO', 'INVALIDATE', 'IS', 'ISOLATION', 'ITEM', 'ITEMS', 'ITERATE', 'JOIN', 'KEY', 'KEYS', 'LAG', 'LANGUAGE', 
			'LARGE', 'LAST', 'LATERAL', 'LEAD', 'LEADING', 'LEAVE', 'LEFT', 'LENGTH', 'LESS', 'LEVEL', 'LIKE', 'LIMIT', 'LIMITED', 'LINES', 'LIST', 'LOAD', 'LOCAL', 'LOCALTIME', 'LOCALTIMESTAMP', 
			'LOCATION', 'LOCATOR', 'LOCK', 'LOCKS', 'LOG', 'LOGED', 'LONG', 'LOOP', 'LOWER', 'MAP', 'MATCH', 'MATERIALIZED', 'MAX', 'MAXLEN', 'MEMBER', 'MERGE', 'METHOD', 'METRICS', 'MIN', 'MINUS', 
			'MINUTE', 'MISSING', 'MOD', 'MODE', 'MODIFIES', 'MODIFY', 'MODULE', 'MONTH', 'MULTI', 'MULTISET', 'NAME', 'NAMES', 'NATIONAL', 'NATURAL', 'NCHAR', 'NCLOB', 'NEW', 'NEXT', 'NO', 'NONE', 
			'NOT', 'NULL', 'NULLIF', 'NUMBER', 'NUMERIC', 'OBJECT', 'OF', 'OFFLINE', 'OFFSET', 'OLD', 'ON', 'ONLINE', 'ONLY', 'OPAQUE', 'OPEN', 'OPERATOR', 'OPTION', 'OR', 'ORDER', 'ORDINALITY', 
			'OTHER', 'OTHERS', 'OUT', 'OUTER', 'OUTPUT', 'OVER', 'OVERLAPS', 'OVERRIDE', 'OWNER', 'PAD', 'PARALLEL', 'PARAMETER', 'PARAMETERS', 'PARTIAL', 'PARTITION', 'PARTITIONED', 'PARTITIONS', 
			'PATH', 'PERCENT', 'PERCENTILE', 'PERMISSION', 'PERMISSIONS', 'PIPE', 'PIPELINED', 'PLAN', 'POOL', 'POSITION', 'PRECISION', 'PREPARE', 'PRESERVE', 'PRIMARY', 'PRIOR', 'PRIVATE', 'PRIVILEGES', 
			'PROCEDURE', 'PROCESSED', 'PROJECT', 'PROJECTION', 'PROPERTY', 'PROVISIONING', 'PUBLIC', 'PUT', 'QUERY', 'QUIT', 'QUORUM', 'RAISE', 'RANDOM', 'RANGE', 'RANK', 'RAW', 'READ', 'READS', 'REAL', 
			'REBUILD', 'RECORD', 'RECURSIVE', 'REDUCE', 'REF', 'REFERENCE', 'REFERENCES', 'REFERENCING', 'REGEXP', 'REGION', 'REINDEX', 'RELATIVE', 'RELEASE', 'REMAINDER', 'RENAME', 'REPEAT', 'REPLACE', 
			'REQUEST', 'RESET', 'RESIGNAL', 'RESOURCE', 'RESPONSE', 'RESTORE', 'RESTRICT', 'RESULT', 'RETURN', 'RETURNING', 'RETURNS', 'REVERSE', 'REVOKE', 'RIGHT', 'ROLE', 'ROLES', 'ROLLBACK', 'ROLLUP', 
			'ROUTINE', 'ROW', 'ROWS', 'RULE', 'RULES', 'SAMPLE', 'SATISFIES', 'SAVE', 'SAVEPOINT', 'SCAN', 'SCHEMA', 'SCOPE', 'SCROLL', 'SEARCH', 'SECOND', 'SECTION', 'SEGMENT', 'SEGMENTS', 'SELECT', 
			'SELF', 'SEMI', 'SENSITIVE', 'SEPARATE', 'SEQUENCE', 'SERIALIZABLE', 'SESSION', 'SET', 'SETS', 'SHARD', 'SHARE', 'SHARED', 'SHORT', 'SHOW', 'SIGNAL', 'SIMILAR', 'SIZE', 'SKEWED', 'SMALLINT', 
			'SNAPSHOT', 'SOME', 'SOURCE', 'SPACE', 'SPACES', 'SPARSE', 'SPECIFIC', 'SPECIFICTYPE', 'SPLIT', 'SQL', 'SQLCODE', 'SQLERROR', 'SQLEXCEPTION', 'SQLSTATE', 'SQLWARNING', 'START', 'STATE', 
			'STATIC', 'STATUS', 'STORAGE', 'STORE', 'STORED', 'STREAM', 'STRING', 'STRUCT', 'STYLE', 'SUB', 'SUBMULTISET', 'SUBPARTITION', 'SUBSTRING', 'SUBTYPE', 'SUM', 'SUPER', 'SYMMETRIC', 'SYNONYM', 
			'SYSTEM', 'TABLE', 'TABLESAMPLE', 'TEMP', 'TEMPORARY', 'TERMINATED', 'TEXT', 'THAN', 'THEN', 'THROUGHPUT', 'TIME', 'TIMESTAMP', 'TIMEZONE', 'TINYINT', 'TO', 'TOKEN', 'TOTAL', 'TOUCH', 
			'TRAILING', 'TRANSACTION', 'TRANSFORM', 'TRANSLATE', 'TRANSLATION', 'TREAT', 'TRIGGER', 'TRIM', 'TRUE', 'TRUNCATE', 'TTL', 'TUPLE', 'TYPE', 'UNDER', 'UNDO', 'UNION', 'UNIQUE', 'UNIT', 
			'UNKNOWN', 'UNLOGGED', 'UNNEST', 'UNPROCESSED', 'UNSIGNED', 'UNTIL', 'UPDATE', 'UPPER', 'URL', 'USAGE', 'USE', 'USER', 'USERS', 'USING', 'UUID', 'VACUUM', 'VALUE', 'VALUED', 'VALUES', 
			'VARCHAR', 'VARIABLE', 'VARIANCE', 'VARINT', 'VARYING', 'VIEW', 'VIEWS', 'VIRTUAL', 'VOID', 'WAIT', 'WHEN', 'WHENEVER', 'WHERE', 'WHILE', 'WINDOW', 'WITH', 'WITHIN', 'WITHOUT', 'WORK', 
			'WRAPPED', 'WRITE', 'YEAR', 'ZONE'
		];

		return this;
	}

	private function getDynamodb()
	{
		return variables.dynamodb;
	}

	private function getTables()
	{
		return variables.tables;
	}

	private function getTable(required string name)
	{
		if (
			!StructKeyExists( getTables() , arguments.name )
		) {

			var table = getDynamodb().getTable(
				arguments.name
			);

			// Just do a describe to check the table exists
			table.describe();

			variables.tables[ arguments.name ] = table;

		}
		return variables.tables[ arguments.name ];
	}

	public any function getItem(required string table, required string key, required string value, boolean asStruct=true, string key2, string value2)
	{
		var table = getTable( 
			name = arguments.table
		);

		if ( StructKeyExists( arguments , 'key2' ) && StructKeyExists( arguments , 'value2' ) ){
			var item = table.getItem(
				arguments.key,
				arguments.value,
				arguments.key2,
				arguments.value2
			);
		} else {
			var item = table.getItem(
				arguments.key,
				arguments.value
			);
		}

		// Set up an empty item if it is null. DynamoDB returns a null object if the document cannot be found by the lookup criteria.
		if ( isNull( local.item ) )
			var item = newItem();
		
		// Return a struct by default
		if ( arguments.asStruct )
			return deserializeJSON( item.toJSON() );

		// Return the item if requested by the `asStruct` arugment
		return item;
	}

	public any function newItem()
	{
		return createAWSObject( 'services.dynamodbv2.document.Item' ).init();
	}

	public any function newItemFromJson(required string json)
	{
		return createAWSObject( 'services.dynamodbv2.document.Item' ).init()
			.fromJson(arguments.json);
	}

	public any function newItemFromMap(required struct map)
	{
		return createAWSObject( 'services.dynamodbv2.document.Item' ).init()
			.fromMap(arguments.map);
	}

	public void function putItem(required string table, required any item )
	{
		var table = getTable(name=arguments.table);

		table.putItem(arguments.item);
	}

	public any function deleteItem(required string table, required string key, required string value, string key2, string value2)
	{
		var table = getTable( 
			name = arguments.table
		);

		if ( StructKeyExists( arguments , 'key2' ) && StructKeyExists( arguments , 'value2' ) ) {
			var deleteItemOutcome = table.deleteItem(
				arguments.key, 
				arguments.value, 
				arguments.key2, 
				arguments.value2 
			);
		} else {
			var deleteItemOutcome = table.deleteItem( 
				arguments.key, 
				arguments.value 
			);
		}

		// Return the outcome
		return deleteItemOutcome;
	}

	public array function scan(required string table, string filterExpression, string projectionExpression, struct nameMap, struct valueMap, boolean asArray=true)
	{
		var table = getTable( 
			name = arguments.table
		);

		var results = table.scan(
			arguments.filterExpression ?: JavaCast( 'null' , 0 ),
			arguments.projectionExpression ?: JavaCast( 'null' , 0 ),
			arguments.nameMap ?: JavaCast( 'null' , 0 ),
			arguments.valueMap ?: JavaCast( 'null' , 0 )
		).iterator();

		if (!arguments.asArray)
			return results;

		var rendered_results = [];

		while ( results.hasNext() ) {
			rendered_results.add(
				deserializeJSON(
					results.next().toJSON()
				)
			);
		}

		return rendered_results;
	}

	public any function getItemWithIndex( required string table, required string indexName, required string key, required string value, string key2, string value2, boolean asStruct=true ){
		var table = getTable( arguments.table );
		var index = table.getIndex( arguments.indexName );
		var query = createAWSObject( "services.dynamodbv2.document.spec.QuerySpec" ).init();

		var keyConditionExpression = "##k1 = :v";

		var nameMap = createAWSObject( "services.dynamodbv2.document.utils.NameMap" ).init()
			.with( "##k1", arguments.key );

		var valueMap = createAWSObject( "services.dynamodbv2.document.utils.ValueMap" ).init()
			.withString( ":v", arguments.value );

		// 2 keys?
		if ( structKeyExists( arguments, "key2" ) && structKeyExists( arguments, "value2" ) ){
			keyConditionExpression &= " and ##k2 = :v2";

			nameMap.with( "##k2", key2 );

			valueMap.withString( ":v2", arguments.value2 );
		}

		var spec = query
			.withKeyConditionExpression( keyConditionExpression )
			.withNameMap( nameMap )
			.withValueMap( valueMap );

		var items = index.query( spec );

		var itr = items.iterator();

		// Set up an empty item if it is null. DynamoDB returns a null object if the document cannot be found by the lookup criteria.
		if ( !itr.hasNext() )
			var item = newItem();
		else
			var item = itr.next();

		// Return a struct by default
		if ( arguments.asStruct )
			return deserializeJSON( item.toJSON() );

		// Return the item if requested by the `asStruct` arugment
		return item;
	}

}